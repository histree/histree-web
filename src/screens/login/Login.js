import React, { Component } from 'react';
import SignInForm from '../../components/sign-in-form/SignInForm'
import SignUpForm from '../../components/sign-up-form/SignUpForm';
import Render from '../../components/ui-components/render-if/Render';
import style from './Login.css';
import { withRouter } from 'react-router-dom';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showLogin : true
        };
        this.changeForm = this.changeForm.bind(this);
    }

    changeForm() {
        this.setState((state) => ({
            showLogin : !state.showLogin
        }));
    }

    render() {
        return (
            <div className={style.background}>
                <Render if={this.state.showLogin}>
                    <SignInForm changeForm={this.changeForm}/>
                </Render>
                <Render if={!this.state.showLogin}>
                    <SignUpForm signIn={this.props.signIn} changeForm={this.changeForm}/>
                </Render>
            </div>
        );
    }
}
export default withRouter(Login);
