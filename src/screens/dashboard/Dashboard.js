import React, { Component } from 'react';
import Render from '../../components/ui-components/render-if/Render'
import ActionBar from '../../components/action-bar/ActionBar'
import Timeline from '../../components/timeline/Timeline';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {},
            isLoading: true
        };
    }

    componentDidMount() { 
        var self = this;
        fetch('http://localhost:3001/dashboard/dashboard', {
            credentials: 'include'
        })
        .then(function(res) {
            res.json()
            .then(function(data) {
                self.setState({
                    user: data.user,
                    isLoading: false
                });
            });
        });
    }

    render() {
        return (
            <Render if={!this.state.isLoading}>
                <ActionBar user={this.state.user}/>
                <Timeline/>
            </Render>
        );
    }
}

export default Dashboard;