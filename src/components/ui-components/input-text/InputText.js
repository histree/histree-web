import React, { Component } from 'react';
import style from './InputText.css';

class InputText extends Component {
    render() {
        return (
            <input type={this.props.type} value={this.props.value} className={style.inputText} onChange={this.props.onChange} placeholder={this.props.placeHolder}/>
        );
    }
}

export default InputText;
