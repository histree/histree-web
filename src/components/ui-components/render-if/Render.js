import { Component } from 'react';

class Render extends Component {
    render() {
        var body = this.props.if ? this.props.children : null;
        return (
            body
        );
    }
}

export default Render;
