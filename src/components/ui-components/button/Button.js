import React, { Component } from 'react';
import style from './Button.css';

class Button extends Component {

    render() {
        return <button className={style.button} disabled={this.props.disabled} onClick={() => this.props.onClick()}>{this.props.text}</button>
    }
}

export default Button;