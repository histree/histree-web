import React, { Component } from 'react';
import style from './Timeline.css'
import Button from '../ui-components/button/Button';
import { withRouter } from 'react-router-dom';

class Timeline extends Component {
    constructor(props) {
        super(props);
        this.state = {
            topics: []
        };
    }

    componentDidMount() {
        var self = this;
        fetch('http://localhost:3001/dashboard/topics', {
            mode: 'cors',
            method: 'POST',
            credentials: 'include',
        })
        .then(function(res) {
            res.json().then(function(data) {
                self.setState({
                    topics: data.topics
                });
            });
        });
    }

    render() {
        var topics = this.state.topics.map(item => {
            return (
                <div>
                    <table>
                        <tr>
                            <td>
                                <div>
                                    <h1>{item.name}</h1>
                                    <h2>{item.description}</h2>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <img src={item.image}/>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            );
        });
        return (
            <div>
               {topics}
            </div>
        );
    }
}

export default withRouter(Timeline);