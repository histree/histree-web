import React, { Component } from 'react';
import { AuthContext } from './AuthContext';
import Render from '../../ui-components/render-if/Render';
import { withRouter } from 'react-router-dom';

class Auth extends Component {
    constructor() {
        super();
        this.state = {
            authenticated: false,
            user: {},
            setAuthenticated: this.setAuthenticated.bind(this),
            setUser: this.setUser.bind(this),
            isLoading: true
        }
    }

    componentDidMount() {
        if (!this.state.authenticated) {
            var self = this;
            fetch('http://localhost:3001/login/authenticated', {
                headers: {
                    'content-type': 'application/json'
                },
                method: "GET",
                mode : "cors",
                credentials: "include"
            })
            .then(function(res) {
                res.json().then(function(data) {
                    if (data.authenticated) {
                        self.setAuthenticated(true);
                        self.props.history.push(self.props.location.pathname);
                    } else {
                        self.props.history.push('/');
                    }
                    self.setState({
                        isLoading: false
                    });
                });
            });
        }
    }

    setAuthenticated = (authenticated) => {
        this.setState({
            authenticated: authenticated
        });
    }

    setUser = (user) => {
        this.setState({
            user: user
        });
    }

    render() {
        return (
            <AuthContext.Provider value={this.state}>
                <div>
                    <Render if={this.state.isLoading}>
                        <div>Loading...</div>
                    </Render>
                    <Render if={!this.state.isLoading}>
                        {this.props.children}
                    </Render>
                </div>
            </AuthContext.Provider>
        );
    }
}

export default withRouter(Auth);
