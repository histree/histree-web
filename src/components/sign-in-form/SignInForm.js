import React, { Component } from 'react';
import InputText from '../ui-components/input-text/InputText';
import style from './SignInForm.css';
import Button from '../ui-components/button/Button';
import { AuthContext } from '../contexts/auth/AuthContext';
import { withRouter } from 'react-router-dom';
import Render from '../ui-components/render-if/Render';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password : '',
            email : '',
            emailValid : false,
            errorMessage: ''
        };
        this.emailChange = this.emailChange.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
        this.signInDisabled = this.signInDisabled.bind(this);
        this.signIn = this.signIn.bind(this);
    }

    emailChange(event) {
        let email = event.target.value;
        this.setState({
            email : email,
            emailValid: false
        });
        if (email) {
            let parts = email.split('@');
            if (parts.length === 2 && parts[0].length > 0 && parts[1].length > 3) {
                let domain = parts[1].split('.');
                if (domain.length > 1 && domain[0].length > 0 && domain[1].length > 0) {
                    this.setState({
                        emailValid : true
                    });
                }
            }
        }
    };

    passwordChange(event) {
        this.setState({
            password : event.target.value
        });
    }

    signInDisabled() {
        return !this.state.emailValid || !this.state.password;
    }

    signIn(authContext) {
        var self = this;
        this.setState({
            errorMessage: ''
        });
        var user = {
            email : this.state.email,
            password : this.state.password
        };
        fetch('http://localhost:3001/login/login', {
            headers: {
                'content-type': 'application/json'
            },
            method: "POST",
            body : JSON.stringify(user),
            mode : "cors",
            credentials: "include"
        })
        .then(function (res) {
            console.log(res);
            if (res.status !== 401) {
                res.json()
                .then(function(data) {
                    authContext.setAuthenticated(true);
                    authContext.setUser(data);
                    self.props.history.push('/dashboard');
                });
            } else {
                self.setState({
                    errorMessage: 'Invalid email and/or password.'
                });
            }
        });
    }

    render() {
        return (
            <AuthContext.Consumer>
                {
                    (authContext) => (
                        <div className={style.container}>
                            <div className={style.header}>
                                <h1 className={style.headerText}>HISTREE</h1>
                            </div>
                            <div className={style.form}>
                                <Render if={this.state.errorMessage}>
                                    <div className={style.error}>{this.state.errorMessage}</div>
                                </Render>
                                <div>
                                    <InputText type="email" value={this.state.email} onChange={this.emailChange} placeHolder="Email"/>
                                </div>
                                <div>
                                    <InputText type="password" value={this.state.password} onChange={this.passwordChange} placeHolder="Password"/>
                                </div>
                                <div>
                                    <div className={style.buttonWrapper}>
                                    <Button disabled={this.signInDisabled()} onClick={() => this.signIn(authContext)} text="Sign In"/>
                                    </div>
                                    <div className={style.buttonWrapper}>
                                        <Button onClick={this.props.changeForm} text="Sign Up"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                }
            </AuthContext.Consumer>
        );
    }
}

export default withRouter(Login);