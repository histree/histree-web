import React, { Component } from 'react';
import InputText from '../ui-components/input-text/InputText';
import Button from '../ui-components/button/Button';
import style from './SignUpForm.css';
import Render from '../ui-components/render-if/Render';

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName : '',
            lastName : '',
            email : '',
            password : '',
            confirmPassword : '',
            errorMessage : '',
            successMessage: ''
        };
        this.firstNameChange = this.firstNameChange.bind(this);
        this.lastNameChange = this.lastNameChange.bind(this);
        this.emailChange = this.emailChange.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
        this.confirmPasswordChange = this.confirmPasswordChange.bind(this);
        this.signUpDisabled = this.signUpDisabled.bind(this);
        this.signUp = this.signUp.bind(this);
    }

    firstNameChange(event) {
        this.setState({
            firstName : event.target.value
        });
    };

    lastNameChange(event) {
        this.setState({
            lastName : event.target.value
        });
    };

    emailChange(event) {
        let email = event.target.value;
        this.setState({
            email : email,
            emailValid : false
        });
        if (email) {
            let parts = email.split('@');
            if (parts.length === 2 && parts[0].length > 0 && parts[1].length > 3) {
                let domain = parts[1].split('.');
                if (domain.length > 1 && domain[0].length > 0 && domain[1].length > 0) {
                    this.setState({
                        emailValid : true
                    });
                }
            }
        }
    };

    passwordChange(event) {
        this.setState({
            password : event.target.value
        });
    }

    confirmPasswordChange(event) {
        this.setState({
            confirmPassword : event.target.value
        });
    }

    signUpDisabled() {
        var disabled =  !this.state.firstName || !this.state.lastName || !this.state.emailValid || !this.state.password || !this.state.confirmPassword || this.state.password !== this.state.confirmPassword;
        return disabled;
    }

    signUp() {
        this.setState({
            errorMessage: ''
        });
        var self = this;
        var user = {
            firstName : this.state.firstName,
            lastName : this.state.lastName,
            email : this.state.email,
            password : this.state.password
        };
        fetch('http://localhost:3001/login/createUser', {
            headers: {
                'content-type': 'application/json'
            },
            method: "POST",
            body : JSON.stringify(user),
            mode : "cors"
        })
        .then(function (res) {
            res.json()
            .then(function(data) {
                if (res.ok) {
                    self.setState({
                        successMessage: data.message
                    });
                } else {
                    self.setState({
                        errorMessage: data.message
                    });
                }
            });
        });
    }

    render() {
        return (
            <div className={style.container}>
                <div className={style.header}>
                    <h1 className={style.headerText}>HISTREE</h1>
                </div>
                <div className={style.form}>
                    <Render if={this.state.errorMessage}>
                        <div className={style.error}>{this.state.errorMessage}</div>
                    </Render>
                    <Render if={this.state.successMessage}>
                        <div className={style.success}>{this.state.successMessage}</div>
                    </Render>
                    <Render if={!this.state.successMessage}>
                        <div>
                            <InputText type="text" value={this.state.firstName} onChange={this.firstNameChange} placeHolder="First Name"/>
                        </div>
                        <div>
                            <InputText type="text" value={this.state.lastName} onChange={this.lastNameChange} placeHolder="Last Name"/>
                        </div>
                        <div>
                            <InputText type="text" value={this.state.email} onChange={this.emailChange} placeHolder="Email"/>
                        </div>
                        <div>
                            <InputText type="password" value={this.state.password} onChange={this.passwordChange} placeHolder="Password"/>
                        </div>
                        <div>
                            <InputText type="password" value={this.state.confirmPassword} onChange={this.confirmPasswordChange} placeHolder="Confirm Password"/>
                        </div>
                        <div className={style.buttonWrapper}>
                            <Button disabled={this.signUpDisabled()} onClick={() => {this.signUp()}} text="Sign Up"/>
                        </div>
                    </Render>
                    <div className={style.buttonWrapper}>
                        <Button onClick={this.props.changeForm} text="Sign In"/>
                    </div>
                </div>
            </div>
        );
    }
}
export default SignUp;