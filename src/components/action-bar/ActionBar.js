import React, { Component } from 'react';
import style from './ActionBar.css'
import Button from '../ui-components/button/Button';
import { withRouter } from 'react-router-dom';

class ActionBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user
        };
        this.logout = this.logout.bind(this);
    }

    logout() {
        var self = this;
        fetch('http://localhost:3001/login/logout', {
            mode: 'cors',
            method: 'POST',
            credentials: 'include',
        })
        .then(function(res) {
            debugger;
            self.props.history.push('/');
        });
    }

    render() {
        return(
            <div className={style.container}>
                <div>
                    <h1 className={style.histree}>Histree</h1>
                </div>
                <div>
                    <div className={style.user}>
                        <Button onClick={this.logout} text="Logout"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(ActionBar);