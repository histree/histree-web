import React, { Component } from 'react';
import Auth from '../components/contexts/auth/Auth';
import { AuthContext } from '../components/contexts/auth/AuthContext';
import { Route, Switch, withRouter } from 'react-router-dom';
import PrivateRoute from '../components/private-route/PrivateRoute';
import Login from '../screens/login/Login';
import Dashboard from '../screens/dashboard/Dashboard';

class App extends Component {

    render() {
        return (
            <Auth>
                <AuthContext.Consumer>
                    {
                        (context) => (
                            <Switch>
                                <Route exact path="/" component={() => <Login />} />
                                <PrivateRoute exact isAuthenticated={context.authenticated} path="/dashboard" component={Dashboard}/>
                            </Switch>
                        )
                    }
                </AuthContext.Consumer>
            </Auth>
        );
    }
}

export default withRouter(App);